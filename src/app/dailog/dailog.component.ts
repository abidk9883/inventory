import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { MatDialogRef } from '@angular/material/dialog'

@Component({
  selector: 'app-dailog',
  templateUrl: './dailog.component.html',
  styleUrls: ['./dailog.component.scss']
})
export class DailogComponent implements OnInit {

  productForm !: FormGroup;

  constructor(private formBuilder : FormBuilder, private api : ApiService, private dialogRef : MatDialogRef<DailogComponent>) { }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      productForm :['',Validators.required],
      Quantity :['',Validators.required],
    URL :['',Validators.required]
    })
  }
  addProduct(){
   if(this.productForm.valid){
    this.api.postProduct(this.productForm.value)
    
    .subscribe({
      next:(res)=>{
        alert("product added sucessfully")
        this.productForm.reset();
        this.dialogRef.close('save');
        
      }
    })
   }
  }
}
